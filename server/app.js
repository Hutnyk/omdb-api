const express = require("express");
const request = require("request");
const cors = require("cors")

const cfg = require("./cfg");
const port = process.env.PORT || cfg.port;

const app = express();
app.use(express.static(__dirname ))
app.use(cors());

// request(`http://www.omdbapi.com/?s=batman&apikey=${cfg.key}&`, {json: true}, (err,res,body)=> {
//     console.log(body);
// })

app.get("/api", (req,res)=> {
    let title = req.query.title
    console.log(title);
    request(`http://www.omdbapi.com/?s=${title}&apikey=${cfg.key}&`, {json: true}, (err,response,body)=> {
    if(err){
        console.log(err)
    }else {
        res.send(body);
    }
})
})



app.listen(port, ()=> {
    console.log(`Hello from port:${port}`);
})
